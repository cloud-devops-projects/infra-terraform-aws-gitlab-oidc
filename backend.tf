terraform {
  backend "s3" {
    bucket  = "tf-backend-projects"
    key     = "infra-terraform-aws-oidc-auth/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
