variable "region" {
  type        = string
  default     = "us-east-1"
  description = "AWS Region"
}

variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "VPC CIDR block"
}

variable "ami" {
  type        = string
  default     = "ami-0cff7528ff583bf9a"
  description = "EC2 instance AMI"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "Instance type"
}
